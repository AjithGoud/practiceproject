package com.user_application.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.user_application.demo.model.User;
import com.user_application.demo.repository.UserRepository;

@RestController
@RequestMapping(value="/user/v1")
public class UserController {
	
	@Autowired
	private UserRepository userrepo;
	

	@GetMapping(value = "/getConnection")
	public String getConnection() {
		RestTemplate rest= new RestTemplate();
		String Url="http://localhost:8080/api/v1/user_controller/check_connectivity";
		ResponseEntity<String> response=rest.getForEntity(Url, String.class);
		System.out.println(response.getBody());
		return response.getBody();
	}
	@PostMapping(value="/ceateUser")
	public String createUser(@RequestBody User user) {
		User u=userrepo.save(user);
		if(u !=null) {
			return "user created";
		}
		return "user not created";
		
		
	}
	@GetMapping(value = "/getUser")
	public List<User> getUserByAge(@RequestParam int age) {
		List<User> users= userrepo.getUserByAge(age);
		return users;
	}
	
	@GetMapping(value = "/getAllUser")
	public List getUserDetails(){
		RestTemplate rest= new RestTemplate();
		String url="http://localhost:8080/api/v1/user_controller/get_users";
		ResponseEntity<List> response= rest.getForEntity(url,List.class);
		return response.getBody();
		
		
	}
}
