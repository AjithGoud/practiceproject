package com.user_application.demo.DTO;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

public class User {

	@JsonProperty("user_id")

	private int user_id;
	@JsonProperty("name")

	private String name;
	@JsonProperty("email")

	private String email;
	@JsonProperty("username")

	private String username;
	@JsonProperty("password")

	private String password;

	@ManyToMany(targetEntity = Roles.class, cascade = CascadeType.ALL)
	private List<Roles> roles;

	public User() {
		super();
	}

	public User(int user_id, String name, String email, String username, String password) {
		super();
		this.user_id = user_id;
		this.name = name;
		this.email = email;
		this.username = username;
		this.password = password;
	}

	public List<Roles> getRoles() {
		return roles;
	}

	public void setRoles(List<Roles> roles) {
		this.roles = roles;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "User [user_id=" + user_id + ", name=" + name + ", email=" + email + ", username=" + username
				+ ", password=" + password + ", roles=" + roles + "]";
	}

}
