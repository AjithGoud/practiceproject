package com.user_application.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.user_application.demo.model.User;

public interface UserRepository extends JpaRepository<User, Integer> {
	
	@Query(value="Select * from user where age=:age",nativeQuery = true)
	List<User> getUserByAge(@Param( "age") int age);
	
}
